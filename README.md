![LogoCarreiras.png](https://bitbucket.org/repo/6bgdoo/images/787302646-LogoCarreiras.png)

**Configurar a conta:HELLEN**
Teste Fernanda Silveira atualizado
Teste Novo diretório - Fernanda

git config --global user.email "you@example.com"

git config --global user.name "Your Name"


**Clonar o repositório**

Copie o link do repositório e cole no seu terminal (Git Bash), acessando as opções a seguir: 

![cronar.jpg](https://bitbucket.org/repo/6bgdoo/images/157346068-cronar.jpg)

**Adicionar arquivo**

git add *    *ou*    git add arquivo

**Atualizar diretório**

git pull

**Comitar o arquivo** 

git commit -m "Mensagem"

**Submitar o arquivo**

git push -u origin branchDesejada

**Alterar branch**

git checkout branchDesejada

**Verificar status**

git status

** Atualizar branches** 

git fetch

**Unificar branch**

git merge origin/branchDesejada

** Estrutura do branch**

![capture_stepup1_5_3.png](https://bitbucket.org/repo/6bgdoo/images/4147822030-capture_stepup1_5_3.png)
