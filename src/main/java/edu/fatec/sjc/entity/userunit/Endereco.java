package edu.fatec.sjc.entity.userunit;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "endereco", catalog = "fatecsjcdb")
public class Endereco implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "endereco_id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "rua", nullable = false)
	private String rua;
	@Column(name = "bairro", nullable = false)
	private String bairro;
	@Column(name = "estado", nullable = false)
	private String estado;
	@Column(name = "cidade", nullable = false)
	private String cidade;
	@Column(name = "cep", nullable = false)
	private String cep;
	@Column(name = "numero", nullable = false)
	private Integer numero;
}
