--liquibase formatted sql

--changeset nadalete:04
--comment: Create USER table structure
CREATE TABLE User (
	user_id	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	nome	TEXT,
	idade	INTEGER
);
--rollback DROP TABLE User;

--changeset nadalete:05
--comment: Create ENDERECO table structure
CREATE TABLE Endereco (
	endereco_id	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	rua	TEXT,
	bairro	TEXT,
	estado	TEXT,
	cidade	TEXT,
	cep	TEXT,
	numero	INTEGER
);
--rollback DROP TABLE Endereco;